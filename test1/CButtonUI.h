#pragma once
#include <afxwin.h>
class CButtonUI :
    public CButton
{
private:
    HBITMAP m_pushbitmap;
    HBITMAP m_nomalbitmap;
    HBITMAP m_hoverbitmap;
    BOOL m_IsinButton;
    BOOL m_IsDownButton ;
public:
    CButtonUI(LPCTSTR lpnormalbitmap, LPCTSTR lphoverbitmap, LPCTSTR lpdownbitmap);
    ~CButtonUI();
    DECLARE_MESSAGE_MAP()
  
    virtual void PreSubclassWindow();
    virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
    afx_msg void OnMouseLeave();
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};

