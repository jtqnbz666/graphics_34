﻿
// DrawDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "Draw.h"
#include "DrawDlg.h"
#include "afxdialogex.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDrawDlg 对话框



CDrawDlg::CDrawDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DRAW_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDrawDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDrawDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CDrawDlg::OnBnClickedOk)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_SIZE()
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()


// CDrawDlg 消息处理程序

BOOL CDrawDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	/*原来
	//CRgn rgn1;
	//rgn1.CreateRectRgn(300, 200, 600, 500);
	//CBrush brush(RGB(0, 100, 120));
	////dc.FillRgn(&rgn1, &brush);

	//CRgn rgn2;
	//rgn2.CreateEllipticRgn(200, 100, 440, 300);
	//CBrush brush2(RGB(0, 199, 200));
	// 
	// 
	//dc.FillRgn(&rgn2, &brush2);*/
	

	//CRgn rgn3;
	//rgn3.CreateRectRgn(0, 0, 0, 0);//必须先创建区域
	////删除了重叠的区域(对区域1和2都造成了删除）
	//rgn3.CombineRgn(&rgn1, &rgn2, RGN_XOR);

	//rgn1.CombineRgn(&rgn1, &rgn2, RGN_XOR);	原来

	

	//设置窗口位不规则窗口
	//细节要点：：
	// 
	//把标题栏删了，分层选择TRUE;
	
	//SetWindowRgn(rgn1, TRUE); // 这行NB了
//直接将窗体弄成rgn3的形状。

	//设置窗口透明度(透明度，透明颜色)
	SetLayeredWindowAttributes(0, 200, LWA_ALPHA);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CDrawDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//CRect rect = { 300,200,500,300 };
		CPaintDC dc(this);
		//使用moveto修改初始划线位置/
		//dc.MoveTo(100, 20);
		//dc.LineTo(200, 40);//画一条线，默认从（0，0）开始画，
		//dc.LineTo(300, 50);//接着上一条线的终点开始画

		//dc.Ellipse(20, 20, 200, 100);//画椭圆

		//绘制圆角矩形
		//dc.RoundRect(20, 20, 200, 100, 30, 30);
		//最后两个参数决定圆角的程度。

		//绘制三维矩形
		//dc.Draw3dRect(&rect,RGB(25,25,25),RGB(200,200,200));

		//绘制带有边缘的矩形
		//BDR_SUNKENINNER下沉风格
		//BDR_RAISEDINNER凸起风格
		//dc.DrawEdge(&rect, BDR_RAISEDINNER, BF_RECT);//

		//绘制楔子（封闭的）
		//CBrush b(RGB(255, 0, 255));
		//CBrush *old = dc.SelectObject(&b);
		////CRect rect = { 200, 200, 400, 300 };
		//CRect rect = { 400, 400, 600, 500 };
		//CPoint centerPt = rect.CenterPoint();
		//
		////CPoint pt1(120, 150), pt2(500, 300);


		//CPoint pt1(320, 350), pt2(700, 500);
		//dc.Pie(&rect, pt1, pt2);
		////原理
		////dc.Rectangle(&rect);
		//dc.MoveTo(pt1);
		//dc.LineTo(pt2);
		//dc.MoveTo(pt1);
		//dc.LineTo(centerPt);
		//dc.LineTo(pt2);
		//dc.SelectObject(old);
		//绘制椭圆弧（未封闭）
		// 这条弧就是楔子的那条弧
		//CRect rect = { 200, 200, 400, 300 };
		//CPoint centerPt = rect.CenterPoint();
		//CPoint pt1(120, 150), pt2(500, 300);
		////原理
		//dc.Rectangle(&rect);
		//dc.MoveTo(pt1);
		//dc.LineTo(centerPt);
		//dc.LineTo(pt2);
		//dc.Arc(&rect, pt1, pt2);


		//绘制椭圆弧，多增加了LineTo的连接线段的功能
		/*CRect rect = { 200, 200, 400, 300 };
		CPoint centerPt = rect.CenterPoint();
		CPoint pt1(120, 150), pt2(500, 300);

		dc.MoveTo(20, 200);
		dc.ArcTo(&rect, pt1, pt2);
		dc.LineTo(300, 300);
		CDialogEx::OnPaint();*/

		//绘制多边形
		CPoint arr[10] = {
			CPoint(601,161),
			CPoint(695, 445),
			CPoint(1001, 445),
			CPoint(751, 627),
			CPoint(845, 915),
			CPoint(601, 735),
			CPoint(355, 918),
			CPoint(447, 632),
			CPoint(201, 454),
			CPoint(505, 445)
		};
		dc.Polygon(arr, 10);//结果是一个五角星
		//dc.Polyline(arr, 10);//不是封闭的五角星，最后一条线没有连接
		//dc.PolylineTo(arr, 10);  //和上一行相比，对了一条从（0，0）开始的线
		//dc.LineTo(300, 300);//接着五角星的最后一条边划线。
		CPoint arr1[6] = {
			CPoint(100,200),
			CPoint(250,80),
			CPoint(120,120),
			CPoint(450,250),
			CPoint(80,100),
			CPoint(400,50)
		};
		DWORD arr2[2] = { 3,3 };

		CBrush b(RGB(255, 0, 255));
		CBrush* old = dc.SelectObject(&b);
		//CRect rect = { 200, 200, 400, 300 };
		CRect rect = { 500, 500, 700, 600 };
		CPoint centerPt = rect.CenterPoint();

		//CPoint pt1(120, 150), pt2(500, 300);


		CPoint pt1(420, 450), pt2(800, 600);
		dc.Pie(&rect, pt1, pt2);
		//原理
		//dc.Rectangle(&rect);
		dc.MoveTo(pt1);
		dc.LineTo(pt2);
		dc.MoveTo(pt1);
		dc.LineTo(centerPt);
		dc.LineTo(pt2);
		dc.SelectObject(old);

		CRect rect1 = { 500, 650, 700, 700 };
		CBrush b1(RGB(0, 0, 255));
		dc.SelectObject(&b1);
		dc.Ellipse(&rect1);
		//dc.PolyPolyline(arr1, arr2, sizeof(arr2)/sizeof(arr2[0]));
		//此处最后一个参数代表的是2，也就是arr2的个数。

		/*CPoint arr3[4] = {
			CPoint(200,500),
			CPoint(240,10),
			CPoint(300, 120),
			CPoint(400, 80)
		};*/
		//dc.PolyBezier(arr3, 4);//绘制一条bezier曲线

		//输出文字
		//dc.SetBkColor(RGB(255, 255, 0));
		//dc.SetBkMode(TRANSPARENT);
		////上边两者选一个用
		//dc.SetTextColor(RGB(0, 255, 255));
		//dc.TextOut(300, 300, L"江涛大帅哥");

		//CString str = L"我是江涛大帅哥";
		//CRect clientRect;
		//GetClientRect(&clientRect);
		//dc.SetTextColor(RGB(255, 0, 0));
		////dc.SetBkMode(TRANSPARENT);
		//dc.DrawText(str, &clientRect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

			//输出图标
		/*dc.DrawIcon(CPoint(50, 40), m_hIcon);
		::DrawIconEx(dc.m_hDC, 500, 500, m_hIcon, 24, 24, 0, NULL, DI_IMAGE | DI_MASK);*/

		//	输出矩形边框
		//CBrush b(RGB(255, 0, 255));
		//CRect rect = { 200, 200, 400, 300 };
		//dc.FrameRect(&rect, &b);
		//dc.DrawText(L"Hello", &rect, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
		//
		////翻转颜色（白变黑，位取反）
		//dc.InvertRect(&rect);

		//绘制一个和弦，运行看效果就知道了，和楔形类似，担有区别。
	/*	CRect rect = { 200, 200, 400, 300 };

		dc.Rectangle(&rect);
		CPoint centerPt = rect.CenterPoint();
		CPoint pt1(120, 150), pt2(500, 300);
		dc.Chord(&rect, pt1, pt2);
		dc.MoveTo(pt1);
		dc.LineTo(pt2);*/
/*
		CRect rect = { 200, 200, 400, 300 };
		CBrush b(RGB(255, 0, 255));
	*/	//dc.FillRect(&rect, &b);

		//dc.FillSolidRect(&rect, RGB(255, 0, 255));

		//区别在于第二个参数不同

		//绘制具有焦点样式的矩形区域
		//dc.DrawFocusRect(&rect);

		//设置一个像素点的颜色
		//dc.SetPixel(CPoint(200, 150), RGB(255, 255, 0));

		//获取一个像素点的颜色
		//COLORREF color=dc.GetPixel(CPoint(200, 150));

		////GDI对象  画笔对象
		//CPen pen1(PS_SOLID, 5, RGB(255, 0, 0));
		//
		//CPen pen2(PS_DASH, 1, RGB(255, 0, 255));
		//CPen pen3(PS_DOT, 5, RGB(255, 0, 255));
		//CPen pen4(PS_NULL, 1, RGB(0, 0, 255));

		//可以同时选画笔和画刷
		/*CPen pen4(PS_DOT, 1, RGB(0, 0, 255));
		CPen* pOldPen = dc.SelectObject(&pen4);*/

		//这个pOldPen会返回上一只笔的类型
		/*CBrush b1(RGB(255, 0, 0));
		dc.SelectObject(b1);
		*///dc.Rectangle(20, 20, 300, 200);
		//pen4.CreatePen();

		//获取画笔的结构
		/*LOGPEN logPen;
		pen4.GetLogPen(&logPen);

		CPen pen6;
		LOGPEN logPen1 = { PS_SOLID, {2,0}, RGB(192, 0, 192) };
		pen6.CreatePenIndirect(&logPen1);
		dc.SelectObject(&pen6);*/
		//dc.Rectangle(50, 50, 80, 80);

		//从画笔句柄转到画笔指针

		/*HPEN hPen = CreatePen(PS_SOLID, 6, RGB(0, 192, 192));
		CPen* pNewPen = CPen::FromHandle(hPen);
		dc.SelectObject(pNewPen);
		dc.Ellipse(200, 200, 600, 400);*/

		////GDI画刷对象
		//CBrush b1(RGB(255, 0, 0));
		//CBrush b2(HS_VERTICAL, RGB(255, 0, 0));

		//CBitmap bmp;
		//bmp.LoadBitmap(IDB_BITMAP1);
		//CBrush b3(&bmp);

		//dc.SelectObject(&b3);
		//dc.Ellipse(200, 200, 600, 400);
		//

		////获取画刷结构
		//LOGBRUSH logBrush;
		//b1.GetLogBrush(&logBrush);

		//CBrush b4;
		//b4.CreateBrushIndirect(&logBrush);
		//
		//CBrush b5;
		////b5.CreateSolidBrush()

		//CBrush b6;
		//b6.CreateSysColorBrush(COLOR_HIGHLIGHT);

		//CBrush* pOldBrush = dc.SelectObject(&b6);
		////dc.Rectangle(0, 0, 600, 400);

		////从画刷句柄转到画刷指针
		//HBRUSH hBrush = CreateSolidBrush(RGB(255, 9, 0));
		//CBrush* pBrush = CBrush::FromHandle(hBrush);

		//GDI对象  ：：位图对象
		//CBitmap	bmp;
		//bmp.LoadBitmap(IDB_BITMAP1);

		////获取位图结构
		//BITMAP  logBmp;
		//bmp.GetBitmap(&logBmp);

		////创建兼容内存DC
		//CDC memDC;
		//memDC.CreateCompatibleDC(&dc);
		//memDC.SelectObject(&bmp);

		////等比列贴图
		////dc.BitBlt(20, 20, 1440, 1080, &memDC, 0, 0, SRCCOPY);

		////拉伸贴图
		////CRect rect;
		//GetClientRect(&rect);
		//dc.SetStretchBltMode(HALFTONE);  //消除红点
		////dc.StretchBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, logBmp.bmWidth, logBmp.bmHeight, SRCCOPY);
		//dc.StretchBlt(0, 0, 100, 100, &memDC, 0, 0, logBmp.bmWidth, logBmp.bmHeight, SRCCOPY);


		////自己创建位图
		//// //CBitmap bmp1;
		////bmp1.CreateCompatibleBitmap(&dc, 400, 300);//纯黑色
		//////创建兼容内存DC
		////CDC memDC;
		////memDC.CreateCompatibleDC(&dc);
		////memDC.SelectObject(&bmp1);

		////memDC.FillSolidRect(0, 0, 400, 300, RGB(255, 255, 255));
		////CPen pen(PS_SOLID, 2, RGB(255,0,0));
		////memDC.SelectObject(&pen);
		////CBrush brush(RGB(255,255,0));
		//// 
		////memDC.SelectObject(brush);
		//// 画刷会把bmp1的颜色刷掉，如果此时获取bmp1的图片，是被刷掉颜色后的样子

		////memDC.Rectangle(20, 20, 300, 200);


		//CImage img;
		//img.Attach(bmp);//绑定
		//img.Save(L"1.png");
		//img.Detach();///断开绑定

		//CFont font;
		//font.CreatePointFont(240, L"楷体");
		//dc.SelectObject(&font);
		//dc.SetBkMode(TRANSPARENT);
		//dc.SetTextColor(RGB(125, 125, 255));
		//dc.TextOut(50, 50, L"江涛大帅哥");

		//CFont font2;
		//font2.CreateFont(
		//	48,
		//	0,
		//	0,
		//	0,
		//	FW_BOLD,
		//	FALSE,
		//	TRUE,
		//	TRUE,
		//	DEFAULT_CHARSET,
		//	OUT_OUTLINE_PRECIS,
		//	CLIP_DEFAULT_PRECIS,
		//	CLEARTYPE_QUALITY,
		//	VARIABLE_PITCH,
		//	TEXT("微软雅黑")
		//);
		//dc.SelectObject(&font2);
		////dc.TextOut(200, 200, L"老铁");
		//
		//LOGFONT logFont;
		//CFont* pFont = GetFont();
		//pFont->GetLogFont(&logFont);
		//logFont.lfHeight = 24;
		//wcscpy(logFont.lfFaceName, L"宋体");

		////创建一个字体
		//CFont font3;
		//font3.CreateFontIndirect(&logFont);
		//dc.SelectObject(&font3);
		////dc.TextOut(200, 300, L"哈哈哈");

		////句柄转指针
		//HFONT hFont = CreateFont(
		//	64,
		//	0,
		//	30,
		//	0,
		//	FW_BOLD,
		//	FALSE,
		//	TRUE,
		//	TRUE,
		//	DEFAULT_CHARSET,
		//	OUT_OUTLINE_PRECIS,
		//	CLIP_DEFAULT_PRECIS,
		//	CLEARTYPE_QUALITY,
		//	VARIABLE_PITCH,
		//	TEXT("黑体")
		//);
		//CFont* pNewFont = CFont::FromHandle(hFont);
		//
		//dc.SelectObject(pNewFont);
		////dc.TextOut(300, 400, L"你好");
		
		

		//不i规则图像
		//CRgn rgn1;
		//rgn1.CreateRectRgn(300, 200, 600, 500);
		//CBrush brush(RGB(0, 100, 120));
		////dc.FillRgn(&rgn1, &brush);

		//CRgn rgn2;
		//rgn2.CreateEllipticRgn(200, 100, 440, 300);
		//CBrush brush2(RGB(0, 199, 200));
		////dc.FillRgn(&rgn2, &brush2);

		////CRgn rgn3;
		////rgn3.CreateRectRgn(0, 0, 0, 0);//必须先创建区域
		//////删除了重叠的区域(对区域1和2都造成了删除）
		////rgn3.CombineRgn(&rgn1, &rgn2, RGN_XOR);

		//rgn1.CombineRgn(&rgn1, &rgn2, RGN_XOR);
		//
		//dc.SelectObject(&rgn1);
		//dc.FillRgn(&rgn1, &brush2);

		
}

	
	
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CDrawDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CDrawDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	//CDialogEx::OnOK();
}


BOOL CDrawDlg::OnEraseBkgnd(CDC* pDC)
{
	//TODO: 在此添加消息处理程序代码和/或调用默认值
	//pDC->SetBkMode(OPAQUE);
	/*pDC->SetBkColor(RGB(255, 255, 0));
	CRect rect = { 300,500,1000,700 };*/
	
	//return TRUE;   //效果和SetBkMode(TRANSPARENT)一样
	//pDC->SetBkMode(TRANSPARENT);
	//pDC->FillSolidRect(&rect, RGB(255, 255, 0));
	//return TRUE;

	return CDialogEx::OnEraseBkgnd(pDC);
}



HBRUSH CDrawDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//pDC->SetBkColor(RGB(255, 255, 0));
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
	
	// TODO:  在此更改 DC 的任何特性

	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}


void CDrawDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	//刷新
	Invalidate(TRUE);//TRUE，表示背景也重画，繁殖

}


LRESULT CDrawDlg::OnNcHitTest(CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	LRESULT lresult= CDialogEx::OnNcHitTest(point);
	if (lresult == HTCLIENT) {
		lresult = HTCAPTION;
	}
	return lresult;
}
