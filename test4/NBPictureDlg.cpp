﻿
// NBPictureDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "NBPicture.h"
#include "NBPictureDlg.h"
#include "afxdialogex.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CNBPictureDlg 对话框



CNBPictureDlg::CNBPictureDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_NBPICTURE_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nIndex = 0;
}

void CNBPictureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CNBPictureDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_NCHITTEST()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CNBPictureDlg 消息处理程序

BOOL CNBPictureDlg::OnInitDialog()
{
	//CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标
	SetTimer(1, 200, NULL);
	::SetWindowPos(m_hWnd,wndTopMost, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);



	return TRUE;
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CNBPictureDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
	
		test();
		CPaintDC dc(this); // 用于绘制的设备上下文
		//显示图片
		//创建内存DC
		CBitmap animation[2];

		animation[m_nIndex].LoadBitmap(IDB_BITMAP1 + m_nIndex);
		BITMAP logBmp;
		animation[m_nIndex].GetBitmap(&logBmp);
		CDC memDC;

		memDC.CreateCompatibleDC(&dc);
		memDC.SelectObject(&animation[m_nIndex]);
		////贴图

		dc.BitBlt(0, 0, logBmp.bmWidth, logBmp.bmHeight, &memDC, 0, 0, SRCCOPY);
		

	//	//CDialogEx::OnPaint();
	}
}
void CNBPictureDlg::test() {
	CBitmap animation[2];

	animation[m_nIndex].LoadBitmap(IDB_BITMAP1 + m_nIndex);
	BITMAP logBmp;
	animation[m_nIndex].GetBitmap(&logBmp);
	//MoveWindow(0, 0, logBmp.bmWidth, logBmp.bmHeight, TRUE);
//创建内存DC
	CDC memDC;
	memDC.CreateCompatibleDC(NULL);
	memDC.SelectObject(&animation[m_nIndex]);

	//创建区域
	CRgn rgnwnd;
	rgnwnd.CreateRectRgn(0, 0, logBmp.bmWidth, logBmp.bmHeight);

	for (int y = 0; y < logBmp.bmHeight; y++)
	{
		for (int x = 0; x < logBmp.bmWidth; x++)
		{
			//一个像素的区域
			CRgn rgn;
			rgn.CreateRectRgn(x, y, x + 1, y + 1);
			if (RGB(255, 255, 255) == memDC.GetPixel(x, y))
			{
				rgnwnd.CombineRgn(&rgnwnd, &rgn, RGN_XOR);
			}
		}
	}
	//SetWindowRgn(rgnwnd, TRUE);

	SetWindowRgn(rgnwnd, TRUE);
	//设置白色透明
	SetLayeredWindowAttributes(RGB(255, 255, 255), 0, LWA_COLORKEY);
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CNBPictureDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



LRESULT CNBPictureDlg::OnNcHitTest(CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	LRESULT lResult = CDialogEx::OnNcHitTest(point);
	if (lResult == HTCLIENT) {
		lResult = HTCAPTION;
	}
	return lResult;
}


void CNBPictureDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	if (nIDEvent == 1)
	{
		m_nIndex++;
		if (m_nIndex >= 2)
			m_nIndex = 0;

		//刷新一下
		Invalidate(FALSE);
	}

	CDialogEx::OnTimer(nIDEvent);
}
