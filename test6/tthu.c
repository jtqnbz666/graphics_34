#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

void myinit(void);
void CALLBACK display(void);
void CALLBACK reshape(GLsizei w,GLsizei h);

//  定义一个一维纹理的数据，从生成来看，保持红色、兰色分量255（MAX），
//  所以是渐变的紫色纹理，饱和度不断变化。
#define TEXTUREWIDTH 64
GLubyte Texture[3*TEXTUREWIDTH];
void makeTexture(void)
{
    int i;
    for(i=0;i<TEXTUREWIDTH;i++)
    {
        Texture[3*i] =255;
        Texture[3*i+1] =255-2*i;
        Texture[3*i+2] =255;
    }
}
GLfloat sgenparams[]={1.0,1.0,1.0,0.0};

void myinit(void)
{
    auxInitDisplayMode(AUX_SINGLE|AUX_RGBA);
    auxInitPosition(0,0,500,500);
    auxInitWindow("sample1");
    glClearColor(0.0,0.0,0.0,0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    //  创建纹理
    makeTexture();
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    //  控制纹理
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
    glTexParameterf(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameterf(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameterf(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexImage1D(GL_TEXTURE_1D,0,3,TEXTUREWIDTH,0,
                GL_RGB,GL_UNSIGNED_BYTE,Texture);
    //  唯一与前面例子不同的地方：启用纹理坐标自动产生，生成环境纹理
    //  纹理的方向S
    glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
    glTexGenfv(GL_S,GL_OBJECT_PLANE,sgenparams);
    //  启用纹理
    glEnable(GL_TEXTURE_1D);
    glEnable(GL_TEXTURE_GEN_S);

    //  启用消隐
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthFunc(GL_LESS);

    //  一些绘图控制，详细可参阅VC5联机帮助
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);
    glFrontFace(GL_CW);
    glCullFace(GL_BACK);
    glMaterialf(GL_FRONT,GL_SHININESS,64.0);
    //  glShadeModel(GL_FLAT);
}

void CALLBACK reshape(GLsizei w,GLsizei h)
{

    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if(w<=h)
        glOrtho(-4.0,4.0,-4.0*(GLfloat)h/(GLfloat)w,
           4.0*(GLfloat)h/(GLfloat)w,-4.0,4.0);
    else
        glOrtho(-4.0*(GLfloat)h/(GLfloat)w,
            4.0*(GLfloat)h/(GLfloat)w,-4.0,4.0,-4.0,4.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void CALLBACK display(void)
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    glRotatef(30.0,1.0,0.0,0.0);
    //  利用辅助库函数绘制一个茶壶
    auxSolidTeapot(1.5);
    glPopMatrix();
    glFlush();
}

void main(void)
{
    myinit();
    auxReshapeFunc(reshape);
    auxMainLoop(display);
}
