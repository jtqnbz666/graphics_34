#include "stdafx.h"
#include "MyApp.h"
#include "MyDlg.h"

//学习的目的：了解Windows程序运行的原理

//MFC:微软封装的Windows的面向对象的C++类库，封装了Windows API
//微软基础类库（Microsoft Foundation Classes）

//优点：可重用性、简单
//缺点：做界面比较丑、如果要做漂亮的程序比较复杂，麻烦

//我们也可以自己封装一个界面库，
//duilib 比较优秀的界面库（迅雷、百度音乐），底层就是Win32

//easyX  不太完善、而且不太符合编程规范，内部采用C++, 官网案例都是以C语言写的、实现一个按钮，没有
//控件这个概念没有

//自己来编写界面库

//三部曲
//第一步：
//建立CWinApp派生类
//第二步：
//重写CWinApp里面的InitInstance虚函数
//第三步：
//定义CWinAPP派生类的全局对象

BOOL CMyApp::InitInstance()
{
	//MessageBox(NULL, L"这是一个MFC程序", L"温馨提示", MB_OK);

	CMyDlg dlg;
	dlg.DoModal();//弹出一个模态对话框

	return TRUE;//初始化成功
}

//执行完之后做的清理工作
int CMyApp::ExitInstance()
{
	return CWinApp::ExitInstance();//返回值是WinMain函数提供的退出代码，return 0;
}



//MFC应用程序核心，一个MFC应用程序有且只有一个应用程序核心对象
CMyApp theApp;



//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPreInstance, LPSTR lpCmdLine, int nCmdShow)
//{
//
//	//HWND类型：窗口句柄， 理解为窗口的ID,一个exe可以多个窗口
//	//HINSTANCE:应用程序实例句柄类型，理解为代表这个exe ,exe
//	MessageBox(NULL, L"这是一个Win32程序", L"温馨提示", MB_OK);
//
//	return 0;
//}