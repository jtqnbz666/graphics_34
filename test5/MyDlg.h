#ifndef _MYDLG_H_
#define _MYDLG_H_
#include"stdAfx.h"
class CMyDlg :public CDialog {


public:
	HICON m_hIcon;
	HICON m_hIcon2;
	CMyDlg();
	BOOL OnInitDialog();
protected:
	const AFX_MSGMAP* GetMessageMap() const;
	static const AFX_MSGMAP* PASCAL GetThisMessageMap();
public:
	afx_msg int OnCreate(LPCREATESTRUCT);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT,int,int);
	afx_msg void OnTimer(UINT_PTR);
	afx_msg void OnLButtonDown(UINT,CPoint);
	

	BOOL PreTranslateMessage(MSG* pMsg);

	void test();
	void OnOK();
	void OnCancel();
//	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
#endif 

