#include "stdafx.h"
#include "MyDlg.h"

BEGIN_MESSAGE_MAP(CMyDlg,CDialog)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_KEYDOWN() //这个没得用的，需要在转换消息的时候截获处理。
	ON_BN_CLICKED(ID_TEST,&CMyDlg::test)
//	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void CMyDlg::OnLButtonDown(UINT wParam, CPoint Point) {
	CString str;
	str.Format(L"当前位置：(%d,%d)", Point.x, Point.y);
	if (wParam & MK_SHIFT) {
		str.Format(L"SHIFT被按下");
	}
	SetWindowText(str);
	return CDialog::OnLButtonDown(wParam, Point);
}
CMyDlg::CMyDlg() :CDialog(IDD_DLG1)
{
	//获取当前应用程序实例句柄
	//AfxGetInstanceHandle()
	//AfxGetApp()->m_hInstance;
	//theApp.m_hInstance
	//AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDI_LOGO));

	m_hIcon = AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDI_LOGO));
	m_hIcon2 = AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDI_ICON1));

}

//响应WM_CREATE消息
//主窗口已经创建，但是子控件还没有创建，窗口还没有显示出来
int CMyDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	MessageBox(L"创建窗口！");
	return CDialog::OnCreate(lpCreateStruct);
}
void CMyDlg::OnClose() {
	MessageBox(L"关闭了窗口！");
	return CDialog::OnClose();
 }
void CMyDlg::OnDestroy() {
	MessageBox(L"销毁了窗口！");
	return CDialog::OnDestroy();
}
void CMyDlg::OnPaint() {
	//MessageBox(L"窗口重绘！");
	return CDialog::OnPaint();
}
void CMyDlg::OnSize(UINT nType,int cx,int cy) {
	//MessageBox(L"窗口大小改变了！");
	CRect rect;
	GetWindowRect(&rect);
	CString str;
	str.Format(L"当前客户区大小：%d*%d", rect.Width(), rect.Height());
	SetWindowText(str);

	//GetClientRect();获取客户区大小。
	//GetWindowRect();获取整个窗口大小。
	return CDialog::OnSize(nType, cx, cy);
}

void CMyDlg::OnTimer(UINT_PTR param) {
	static int flag = 1;
	switch (param)
	{
	case 1: {
		if (flag == 1) {
			SetIcon(m_hIcon, TRUE);
			SetIcon(m_hIcon, FALSE);
			flag = 0;
		}
		else {
			SetIcon(m_hIcon2, TRUE);
			SetIcon(m_hIcon2, FALSE);
			flag = 1;
		}
	}
		  break;
	case 2: {
		CTime time = CTime::GetCurrentTime();
		CString str = time.Format("当前时间：%Y-%m-%d %H:%M:%S %B ");
		SetWindowText(str);
	}
	default:
		break;
	}
	return CDialog::OnTimer(param);
}

void CMyDlg::test() {
	MessageBox(L"点击了测试按钮！");

}
BOOL CMyDlg::PreTranslateMessage(MSG* pMsg) {
	if (pMsg->message == WM_KEYDOWN) {
		if (pMsg->wParam == VK_CONTROL) {
			MessageBox(L"按下了CONTROL键！");
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CMyDlg::OnOK() {
	MessageBox(L"点击了OK");
	return CDialog::OnOK();
}
void CMyDlg::OnCancel() {
	MessageBox(L"点击了Cancel");
	return CDialog::OnCancel();
}
///初始化对话框
//主窗口已经创建好了，子控件也创建好了，窗口还没有显示出来而已


BOOL CMyDlg::OnInitDialog()
{
	//SetWindowText(L"江涛老表");

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);
	DWORD dwStyle = GetWindowLong(m_hWnd, GWL_STYLE);
	dwStyle = dwStyle | WS_THICKFRAME;

	SetWindowLong(m_hWnd, GWL_STYLE, dwStyle);
	SetTimer(1, 200, NULL);
	SetTimer(2, 1000, NULL);
	////CString 类
	////Win32是以C语言的语法实现
	////char*
	////const char*
	////LPCTSTR
	////LPSTR
	//
	//CString str = L"12345678";

	//CString str1(L"jt");

	////获取一个字符串的长度
	////nLength长度是指字符数
	////Unicode编码下面：4
	//int nLength = str.GetLength();

	////清空一个字符串
	////str.Empty();

	////判断字符串是不是为空
	//if (str.IsEmpty())
	//{
	//	//MessageBox(L"为空");
	//}
	//else
	//{
	//	//MessageBox(L"不为空");
	//}

	////wchar_t ch = str.GetAt(1);
	//wchar_t ch = str[1];

	////str.SetAt(0,'5');

	//if (str == str1)
	//{
	//	MessageBox(L"相等");
	//}

	////字符串的截取
	//CString newStr = str.Mid(2);
	//newStr = str.Left(5);
	//newStr = str.Right(5);

	////字符串大小写转化
	//CString str2 = L"Absrhyi5445";
	//newStr = str2.MakeUpper();
	//newStr = str2.MakeLower();
	////反转字符串
	//newStr = str2.MakeReverse();

	////格式化字符串
	////sprintf
	////wsprintf
	//str2.Format(L"   当前时间:%d-%d-%d %d:%d:%d    ", 2022, 4, 26, 22, 28, 30);

	////替换
	//str2.Replace(L"-", L".");

	////字符串查找
	//int nPos = str2.Find(L"-");

	////就是查找最后一个
	//nPos = str2.ReverseFind(':');

	//if (nPos == -1)
	//{
	//	MessageBox(L"没找到");
	//}
	//else
	//{
	//	str2.Format(L"%s   在当前位置：%d", str2, nPos);
	//}

	////去空格
	//nLength = str2.GetLength();
	//str2 = str2.Trim();

	//nLength = str2.GetLength();

	////
	//str2.TrimLeft();
	//str2.TrimRight();

	////案例：
	////有一个路径,获取文件的文件名以及包含拓展名
	////D:\Program Files\KuGou\KGMusic\kugou.exe

	//CString strPath = L"D:\\Program Files\\KuGou\\KGMusic\\kugou.exe";

	//nPos = strPath.ReverseFind(L'\\');
	//CString strExeName;
	//if (nPos != -1)
	//{
	//	strExeName = strPath.Mid(nPos + 1);
	//}

	////CString怎么转 const char*
	////没有直接转C++字符串的

	//SetWindowText(strExeName.GetBuffer());

	return TRUE;
}

//void CMyDlg::OnMouseMove(UINT nFlags, CPoint point)
//{
//	// TODO: 在此添加消息处理程序代码和/或调用默认值
//
//	CDialog::OnMouseMove(nFlags, point);
//}



